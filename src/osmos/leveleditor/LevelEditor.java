package osmos.leveleditor;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.Optional;

import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.scene.Cursor;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SeparatorMenuItem;
import javafx.scene.control.Slider;
import javafx.scene.control.TextInputDialog;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.util.Pair;
import osmos.model.Cell;
import osmos.model.CellColor;
import osmos.model.Effect;
import osmos.view.GameMenu;

public class LevelEditor extends Pane{
	private final static String LEVELS_DIR = "./levels/";
	private final static double RESIZE_REGION_THRESHOLD = 0.85;
	private ArrayList<Cell> cells = new ArrayList<>();
	private GameMenu menu;
	private Scene scene;
	private CellContextMenu cellContextMenu = new CellContextMenu();
	private ContextMenu contextMenu = new ContextMenu();
	private Cell playerCell;
	private TextInputDialog effectTimeDialog = new TextInputDialog();
	{
		effectTimeDialog.setTitle("Effect Time");
		effectTimeDialog.setHeaderText("Input time for the effect");
		effectTimeDialog.setContentText("Time(s)");
		effectTimeDialog.getEditor().setText("10");
		MenuItem save = new MenuItem("Save");
		MenuItem exit = new MenuItem("Exit");
		contextMenu.getItems().addAll(save,exit);
		save.setOnAction((e)->{
			save();
		});
		exit.setOnAction((e)->{
			scene.setRoot(menu);
			menu.show();
		});
	}
	private TextInputDialog nameDialog = new TextInputDialog();
	{
		nameDialog.setTitle("Level name");
		nameDialog.setHeaderText("Input name of the level (only alphanumeric characters)");
		nameDialog.setContentText("Name");
		nameDialog.getEditor().setText("New Level");
	}
	public LevelEditor(GameMenu menu, Scene scene){
		this.menu = menu;
		this.scene = scene;
		this.setBackground(new Background(new BackgroundFill(Color.BLACK, null, null)));
				
		addEventFilter(MouseEvent.ANY, new EventHandler<MouseEvent>(){
			boolean drag = false;
			double sx = 0;
			double sy = 0;
			Cell draggedCell;
			@Override
			public void handle(MouseEvent event) {
				if(event.getEventType() == MouseEvent.MOUSE_PRESSED){
					cellContextMenu.hide();
					contextMenu.hide();
				}
				if(event.getEventType() == MouseEvent.MOUSE_DRAGGED && draggedCell != null){
					if(drag){
						draggedCell.x(event.getSceneX() - sx);
						draggedCell.y(event.getSceneY() - sy);
						draggedCell.cellView().update();
					}else{
						double dx = event.getSceneX() - draggedCell.x();
						double dy = event.getSceneY() - draggedCell.y();
						double d = Math.sqrt(dx*dx + dy*dy);
						draggedCell.radius(d);
						draggedCell.cellView().update();
					}
				}else {
					for(Cell cell : cells){
						double dx = event.getSceneX() - cell.x();
						double dy = event.getSceneY() - cell.y();
						double d = Math.sqrt(dx*dx + dy*dy);
						
						if(d > cell.radius()){
							cell.cellView().setCursor(Cursor.DEFAULT);
							continue;
						}
						if(event.getEventType() == MouseEvent.MOUSE_MOVED){
							if(d < RESIZE_REGION_THRESHOLD * cell.radius()){
								cell.cellView().setCursor(Cursor.OPEN_HAND);
							}else{
								double alpha = Math.toDegrees(Math.atan2(dx, dy));
								if(alpha > 67.5 && alpha < 112.5|| alpha < -67.5 && alpha > -112.5){
									cell.cellView().setCursor(Cursor.E_RESIZE);
								}else if(alpha >= 112.5 && alpha < 157.5 || alpha <= -22.5 && alpha > -67.5){
									cell.cellView().setCursor(Cursor.NE_RESIZE);
								}else if(alpha >= 22.5 && alpha < 67.5 || alpha <= -112.5 && alpha > -157.5){
									cell.cellView().setCursor(Cursor.NW_RESIZE);
								}else{
									cell.cellView().setCursor(Cursor.N_RESIZE);
								}
							}
						}else if(event.getEventType() == MouseEvent.MOUSE_PRESSED){
							cell.cellView().setCursor(Cursor.CLOSED_HAND);
							if(d < RESIZE_REGION_THRESHOLD * cell.radius()){
								sx = event.getSceneX() - cell.x();
								sy = event.getSceneY() - cell.y();
								drag = true;
							}else{
								drag = false;
							}
							draggedCell = cell;
						}else if(event.getEventType() == MouseEvent.MOUSE_RELEASED){
							draggedCell = null;
						}else if(event.getEventType() == MouseEvent.MOUSE_CLICKED && event.getButton() == MouseButton.SECONDARY){
							cellContextMenu.delete.setDisable(cell == playerCell);
							cellContextMenu.show(cell,event);
							return;
						}else if(event.getEventType() == MouseEvent.MOUSE_CLICKED && event.getButton() == MouseButton.PRIMARY){
							return;
						}
					}
					
					if(event.getEventType() == MouseEvent.MOUSE_CLICKED){
						if(event.getButton() == MouseButton.PRIMARY){
							Cell cell = new Cell(event.getSceneX(), event.getSceneY(), 40.0);
							if(playerCell == null){
								playerCell = cell;
								cell.color(CellColor.BLUE);
							}
							cells.add(cell);
							LevelEditor.this.getChildren().add(cell.cellView());
							
							cell.cellView().update();
						}else if(event.getButton() == MouseButton.SECONDARY){
							contextMenu.show(LevelEditor.this, event.getScreenX(), event.getScreenY());
						}
					}
				}
			}
		});
	}
	
	private void save() {
		String name;
		if((name = getName()) == null)
			return;
		try (BufferedWriter writer = Files.newBufferedWriter(
				new File(LEVELS_DIR + name.replaceAll(" ", "_").concat(".lvl")).toPath(), 
				StandardOpenOption.CREATE)){
			
			writer.write(name + System.lineSeparator());
			writer.write(playerCell.toSaveString() + System.lineSeparator() );
			for(Cell cell : cells){
				if(cell != playerCell)
					writer.write(cell.toSaveString() + System.lineSeparator() );
			}
			writer.flush();
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private String getName(){
		String name = null;
		do{
			Optional<String> result = nameDialog.showAndWait();
			if(!result.isPresent())
				return null;
			name = result.get();
		}while(name.equals("") || name.contains("[^a-zA-Z0-9]"));
		return name;
	}

	private class CellContextMenu extends ContextMenu{
		private Menu cellTypeMenu = new Menu("Cell Type");
		private MenuItem normal = new MenuItem("Normal");
		private MenuItem player = new MenuItem("Player Cell");
		private SeparatorMenuItem separator = new SeparatorMenuItem();
		private MenuItem immortality = new MenuItem("Immortality");
		private MenuItem unlimitedAmmo = new MenuItem("Unlimited Ammo");
		private MenuItem stun = new MenuItem("Stun");
		private MenuItem freeze = new MenuItem("Freeze");
		
		private MenuItem velocity = new MenuItem("Velocity");
		private MenuItem delete = new MenuItem("Delete");
			
		public CellContextMenu(){
			cellTypeMenu.getItems().addAll(normal,player,separator,immortality,unlimitedAmmo,stun,freeze);
			this.getItems().addAll(cellTypeMenu,velocity,delete);
		}
		
		public void show(Cell cell, MouseEvent event){
			normal.setOnAction((e)->{
				if(cell == playerCell){
					playerCell = null;
				}
				cell.effect(Effect.NONE,0,false);
				cell.cellView().update();
			});
			player.setOnAction((e)->{
				if(playerCell != null){
					playerCell.color(CellColor.GREY);
					playerCell.cellView().update();
				}
				playerCell = cell;
				cell.effect(Effect.NONE,0,false);
				cell.color(CellColor.BLUE);
				cell.cellView().update();
			});
			immortality.setOnAction((e)->{
				if(cell == playerCell){
					playerCell = null;
				}
				double time;
				if((time = getTime()) > 0)
					cell.effect(Effect.IMMORTAL,time,true);
				cell.cellView().update();
			});
			unlimitedAmmo.setOnAction((e)->{
				if(cell == playerCell){
					playerCell = null;
				}
				double time;
				if((time = getTime()) > 0)
					cell.effect(Effect.UNLIMITTED_AMMO,time,true);
				cell.cellView().update();
			});
			stun.setOnAction((e)->{
				if(cell == playerCell){
					playerCell = null;
				}
				double time;
				if((time = getTime()) > 0)
					cell.effect(Effect.STUN,time,true);
				cell.cellView().update();
			});
			freeze.setOnAction((e)->{
				if(cell == playerCell){
					playerCell = null;
				}
				double time;
				if((time = getTime()) > 0)
					cell.effect(Effect.FREEZE,time,true);
				cell.cellView().update();
			});
			velocity.setOnAction((e)->{
				Dialog<Pair<Double,Double>> dialog = new Dialog<>();
				dialog.setTitle("Vertical velocity");
				dialog.setHeaderText("Input ");
				dialog.getDialogPane().getButtonTypes().addAll(ButtonType.OK, ButtonType.CANCEL);
				
				GridPane grid = new GridPane();
				grid.setHgap(10);
				grid.setVgap(10);
				grid.setPadding(new Insets(20,150,10,10));
				
				Slider hSlider = new Slider(-100, 100, cell.vx());
				Slider vSlider = new Slider(-100, 100, cell.vy());
				hSlider.setShowTickLabels(true);
				vSlider.setShowTickLabels(true);
				vSlider.setOrientation(Orientation.VERTICAL);
				
				grid.add(new Label("Vx"), 0, 0);
				grid.add(hSlider, 1, 0);
				grid.add(new Label("Vy"), 0, 1);
				grid.add(vSlider, 1, 1);
				
				dialog.getDialogPane().setContent(grid);
				
				dialog.setResultConverter((dialogButton)->{
					if(dialogButton == ButtonType.OK){
						return new Pair<>(hSlider.getValue(), vSlider.getValue());
					}
					return null;
				});
				
				Optional<Pair<Double,Double>> result = dialog.showAndWait();
				result.ifPresent(vel -> {
					cell.vx(vel.getKey());
					cell.vy(-vel.getValue());
				});
			});
			delete.setOnAction((e)->{
				cells.remove(cell);
				LevelEditor.this.getChildren().remove(cell.cellView());
				cell.cellView().update();
			});
			this.show(LevelEditor.this,event.getScreenX(), event.getScreenY());
		}
		
		private double getTime(){
			double time = 0;
			do{
				Optional<String> result = effectTimeDialog.showAndWait();
				if(!result.isPresent())
					return -1;
				try{
					time = Double.parseDouble(result.get());
				}catch(NumberFormatException ex){
					Alert alert = new Alert(AlertType.WARNING);
					alert.setTitle("Warning");
					alert.setTitle("Wrong input");
					alert.setContentText("Input should be a number");
				}
			}while(time == 0);
			return time;
		}
	}
}
