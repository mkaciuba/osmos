package osmos.controller;

import java.util.ArrayList;
import java.util.concurrent.ConcurrentLinkedQueue;

import javafx.animation.AnimationTimer;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.input.MouseEvent;
import javafx.util.Pair;
import osmos.model.Cell;
import osmos.model.Effect;
import osmos.view.Board;
import osmos.view.CellView;
import osmos.view.GameMenu;

public class MainController extends AnimationTimer{
	public static final double FRAME_TIME = 0.01666666666666;
	private static final long END_DELAY = 5000000000l;
	private long prevTime = 0;
	private ArrayList<Cell> cells = new ArrayList<>();
	private ArrayList<CellView> cellViews = new ArrayList<>();
	private ConcurrentLinkedQueue<Pair<Double, Double>> clickQueue = new ConcurrentLinkedQueue<>(); 
	private Cell playerCell;
	private final Board board;
	private final GameRules rules;
	private final GameMenu menu;
	private final Scene scene;
	private boolean end;
	public MainController(Board board, GameMenu menu, Scene scene, GameRules rules, ArrayList<Cell> cells, Cell playerCell){
		this.board = board;
		this.rules = rules;
		this.menu = menu;
		this.scene = scene;
		for(Cell cell : cells){
			registerCell(cell);
		}
		this.playerCell = playerCell;
		registerCell(playerCell);
		board.addEventFilter(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent event) {
				clickQueue.add(new Pair<Double,Double>(event.getSceneX(),event.getSceneY()));
			}
		});
	}

	private void calculateFrame(double dt) {
		for(Cell cell : cells){
			if(playerCell.effect() != Effect.FREEZE || cell.equals(playerCell))
				cell.update(dt);
		}
		bounce();
		collide();
		shoot();
		for(CellView cellView : cellViews){
			cellView.update();
		}
		board.updateStatus(playerCell);
		GameStatus status = rules.checkGameStatus(playerCell, cells);
		if(status != GameStatus.NORMAL){
			board.endGame(status);
			end = true;
		}
	}	
	
	private void shoot() {
		while(!clickQueue.isEmpty()){
			Pair<Double, Double> pair = clickQueue.poll();
			if(playerCell.effect() != Effect.STUN)
				registerCell(playerCell.shoot(pair.getKey(), pair.getValue()));
		}
	}

	private void bounce(){
		for(Cell cell : cells){
			if(cell.x() <= cell.radius()){
				cell.x(2*cell.radius() - cell.x());
				cell.vx(-cell.vx());
			}
			if(cell.x() >= board.width() - cell.radius()){
				cell.x(2*(board.width() - cell.radius()) - cell.x());
				cell.vx(-cell.vx());
			}
			if(cell.y() <= cell.radius()){
				cell.y(2*cell.radius() - cell.y());
				cell.vy(-cell.vy());
			}
			if(cell.y() >= board.height() - cell.radius()){
				cell.y(2*(board.height() - cell.radius()) - cell.y());
				cell.vy(-cell.vy());
			}
		}
	}
	
	private void collide(){
		ArrayList<Cell> deadCells = new ArrayList<>();
		for(int i=0; i<cells.size(); i++){
			Cell c1 = cells.get(i);
			for(int j=i+1; j<cells.size(); j++){
				Cell c2 = cells.get(j);
				double dx = c2.x() - c1.x();
				double dy = c2.y() - c1.y();
				double d = Math.sqrt(dx*dx + dy*dy);
				
				if(d < c1.radius() + c2.radius()){
					double overlap = c2.radius() + c1.radius() - d;
					Cell bigger, smaller;
					if(c2.radius() > c1.radius()){
						bigger = c2;
						smaller = c1;
					}else{
						bigger = c1;
						smaller = c2;
					}
					if(!(smaller.equals(playerCell) && playerCell.effect() == Effect.IMMORTAL)){
						overlap = Math.min(smaller.radius(), overlap);
						double mass = Cell.bite(smaller.radius(), overlap);
						if(smaller.effect() != Effect.NONE){
							double deltaEffectTime = smaller.effectTimeBite(mass);
							smaller.addEffectTime(-deltaEffectTime);
							bigger.effect(smaller.effect(),deltaEffectTime, false);
						}
						if(bigger.equals(playerCell) && playerCell.effect() == Effect.FREEZE){
							smaller.deltaMass(-mass,0,0);
							bigger.deltaMass(mass,0,0);
						}else{
							smaller.deltaMass(-mass,smaller.vx(),smaller.vy());
							bigger.deltaMass(mass,smaller.vx(),smaller.vy());
						}
						if(!smaller.isAlive())
							deadCells.add(smaller);
						
						
					}
				}
			}
		}
		for(Cell dead : deadCells){
			unregisterCell(dead);
		}
	}
	
	private void registerCell(Cell cell){
		board.getChildren().add(cell.cellView());
		cellViews.add(cell.cellView());
		cells.add(cell);
	}
	
	private void unregisterCell(Cell cell) {
		cells.remove(cell);
		cellViews.remove(cell.cellView());
		board.getChildren().remove(cell.cellView());
	}

	@Override
	public void handle(long now) {
		if(end && now - prevTime >= END_DELAY){
			stop();
			scene.setRoot(menu);
			menu.show();
		}else if(!end){
			if(prevTime == 0){
				prevTime = now;
				return;
			}
			double dt = (now - prevTime)/1000000000.0;
			
			while(dt > FRAME_TIME){
				dt -= FRAME_TIME;
				calculateFrame(FRAME_TIME);
				prevTime = now;
			}
		}
	}
}
