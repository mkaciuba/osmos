package osmos.controller;

import java.util.List;

import osmos.model.Cell;

public interface GameRules {
	public GameStatus checkGameStatus(Cell playerCell,List<Cell> cells);
}
