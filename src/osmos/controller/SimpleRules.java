package osmos.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import osmos.model.Cell;

public class SimpleRules implements GameRules{

	@Override
	public GameStatus checkGameStatus(Cell playerCell, List<Cell> cells) {
		List<Cell> sortedCells = new ArrayList<>(cells);
		Collections.sort(sortedCells, new Comparator<Cell>(){
			@Override
			public int compare(Cell c1, Cell c2) {
				return Double.compare(c1.radius(), c2.radius());
			}
		});
		
		double massSum = 0;
		for(Cell cell : sortedCells){
			if(cell.equals(playerCell)){
				massSum += playerCell.mass();
			}
			if(sortedCells.indexOf(cell) > sortedCells.indexOf(playerCell) && massSum < cell.mass())
				return GameStatus.LOOSE;
			massSum += cell.mass();
		}
		
		if(sortedCells.indexOf(playerCell) == sortedCells.size() - 1)
			return GameStatus.WIN;
		
		return GameStatus.NORMAL;
	}

}
