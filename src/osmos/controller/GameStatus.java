package osmos.controller;

public enum GameStatus {
	NORMAL,
	WIN,
	LOOSE
}
