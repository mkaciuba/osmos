package osmos.model;

public enum Effect {
	NONE(0),
	FREEZE(1),
	STUN(2),
	IMMORTAL(3),
	UNLIMITTED_AMMO(4);
	
	private int id;
	
	Effect(int id){
		this.id = id;
	}
	
	public int id(){
		return id;
	}
	
	public static Effect fromId(int id){
		switch(id){
		case 0:
			return NONE;
		case 1:
			return FREEZE;
		case 2:
			return STUN;
		case 3:
			return IMMORTAL;
		case 4:
			return Effect.UNLIMITTED_AMMO;
		default:
			return NONE;
		}
	}
}
