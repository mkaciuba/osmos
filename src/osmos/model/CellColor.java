package osmos.model;

import javafx.scene.paint.Color;

public enum CellColor {
	WHITE(0,1,1,1),
	BLUE(1,0.098,0.239,0.565),
	RED(2,0.773,0.137,0.137),
	YELLOW(3,0.847,0.584,0.067),
	GREEN(4,0.051,0.675,0.051),
	GREY(5,0.45,0.45,0.45), 
	PURPLE(6,0.51,0.169,0.4);
	
	private Color value;
	private int id;
	
	private CellColor(int id, double r, double g, double b){
		value = new Color(r, g, b, 0.8);
		this.id = id;
	}
	
	public Color value(){
		return value;
	}
	
	public int id(){
		return id;
	}
	
	public static CellColor fromId(int id){
		switch(id){
		case 0:
			return WHITE;
		case 1:
			return BLUE;
		case 2:
			return RED;
		case 3:
			return YELLOW;
		case 4:
			return GREEN;
		case 5:
			return GREY;
		case 6:
			return PURPLE;
		default:
			return GREY;
		}
	}
}
