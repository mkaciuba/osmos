package osmos.model;

import java.util.Scanner;

import osmos.view.CellView;

public class Cell {

	public static final double DENSITY = 1.0;
	public static final double PROJECTILE_VELOCITY = 600.0;
	public static final double PROJECTILE_RADIUS_RATIO = 0.1;
	public static final double DEATH_THRESHOLD = 3.0;
	private double x;
	private double y;
	private double vx;
	private double vy;
	private double radius;
	private boolean alive = true;
	private CellColor color;
	private CellColor defaultColor;
	private CellView cellView;
	private Effect effect = Effect.NONE;
	private boolean effectCell;
	private double effectTimer = 0;
	
	public Cell(double x, double y, double radius){
		this(x,y,0,0,radius);
	}
	public Cell(double x, double y, double radius, CellColor color){
		this(x,y,0,0,radius,CellColor.GREY);
	}
	public Cell(double x, double y, double vx, double vy, double radius){
		this(x,y,vx,vy,radius,CellColor.GREY);
	}
	public Cell(double x, double y, double vx, double vy, double radius, CellColor color){
		this(x,y,vx,vy,radius,color, Effect.NONE, 0, false);
	}
	public Cell(double x, double y, double vx, double vy, double radius, CellColor color, Effect effect, double effectTime, boolean effectCell){
		this.x = x;
		this.y = y;
		this.vx = vx;
		this.vy = vy;
		this.radius = radius;
		this.color = color;
		this.defaultColor = color;
		this.effect = effect;
		this.effectTimer = effectTime;
		this.effectCell = effectCell;
		this.cellView = new CellView(this);
	}
	
	public void update(double dt){
		x += dt*vx;
		y += dt*vy;
		if(!effectCell)
			effectTimer -= dt;
		if(effectTimer < 0){
			effectTimer = 0;
			effect = Effect.NONE;
			color = defaultColor;
		}
	}
	
	public void deltaMass(double mass, double mvx, double mvy){
		double currentMass = Math.PI * radius * radius * DENSITY;
		double newMass = currentMass + mass;
		if(newMass <= 0){
			alive = false;
			radius = 0;
			return;
		}
		vx = (currentMass * vx + mass * mvx)/newMass;
		vy = (currentMass * vy + mass * mvy)/newMass;
		
		radius = Math.sqrt(newMass/(Math.PI * DENSITY));
	}
	
	public Cell shoot(double tx, double ty){
		double dx = tx - x;
		double dy = ty - y;
		double d = Math.sqrt(dx*dx + dy*dy);
		double pvx = PROJECTILE_VELOCITY * dx / d;
		double pvy = PROJECTILE_VELOCITY * dy / d;
		double pr = radius * PROJECTILE_RADIUS_RATIO;
		double dr = (radius + pr) * 1.05;
		double px = x + dr * dx / d;
		double py = y + dr * dy / d;
		
		if(effect != Effect.UNLIMITTED_AMMO)
			deltaMass(-Math.PI*pr*pr*DENSITY, pvx, pvy);
		
		return new Cell(px,py,pvx,pvy,pr);
	}
	
	public Effect effect(){
		return effect;
	}
	
	public double effectTime(){
		return effectTimer;
	}
	
	public double effectTimeBite(double mass){
		double totalMass = Math.PI * radius * radius * DENSITY;
		return effectTimer * mass / totalMass;
	}
	
	public void addEffectTime(double time){
		effectTimer += time;
	}
	
	public void effect(Effect effect, double time, boolean effectCell){
		if(effectCell){
			this.effect = effect;
			this.effectTimer = time;
			this.effectCell = true;
		}else if(effect == this.effect){
			effectTimer += time;
		}else{
			effectTimer = time;
			this.effect = effect;			
		}
		switch(effect){
		case FREEZE:
			color = CellColor.RED;
			break;
		case IMMORTAL:
			color = CellColor.YELLOW;
			break;
		case NONE:
			color = defaultColor;
			break;
		case STUN:
			color = CellColor.PURPLE;
			break;
		case UNLIMITTED_AMMO:
			color = CellColor.GREEN;
			break;
		default:
			break;
		
		}
	}
	
	public double x(){
		return x;
	}
	
	public double y(){
		return y;
	}
	public void x(double x){
		this.x = x;
	}
	
	public void y(double y){
		this.y = y;
	}
	
	public double vx(){
		return vx;
	}
	
	public double vy(){
		return vy;
	}
	public void vx(double vx){
		this.vx = vx;
	}
	
	public void vy(double vy){
		this.vy = vy;
	}
	
	public boolean isAlive(){
		return alive;
	}
	
	public CellColor color(){
		return color;
	}
	
	public void color(CellColor color){
		this.color = color;
	}

	public double radius() {
		return radius;
	}
	
	public void radius(double radius) {
		this.radius = radius;
	}
	
	public CellView cellView(){
		return cellView;
	}
	
	public double mass(){
		return bite(radius, radius);
	}
	
	public static double bite(double radius, double overlap){
		if(overlap > radius - DEATH_THRESHOLD)
			overlap = radius;
		double smallRadius = radius - overlap;
		double a = Math.PI * radius * radius - Math.PI * smallRadius * smallRadius;
		return a * DENSITY;
	}

	@Override
	public String toString() {
		return "Cell [x=" + x + ", y=" + y + ", vx=" + vx + ", vy=" + vy + ", radius=" + radius + ", alive=" + alive
				+ ", color=" + color + ", defaultColor=" + defaultColor + ", cellView=" + cellView + ", effect="
				+ effect + ", effectCell=" + effectCell + ", effectTimer=" + effectTimer + "]";
	}
	
	public String toSaveString() {
		String result = x + " " + y + " " + vx + " " + vy + " " + radius
				+ " " + color.id() + " " 
				+ effect.id() + " " + effectTimer + " " + effectCell;
		return result.replace('.', ',');
	}
	public static Cell parse(String line) {
		try(Scanner s = new Scanner(line)){
			return new Cell(
					s.nextDouble(),
					s.nextDouble(),
					s.nextDouble(),
					s.nextDouble(),
					s.nextDouble(),
					CellColor.fromId(s.nextInt()),
					Effect.fromId(s.nextInt()),
					s.nextDouble(),
					s.nextBoolean()
					);
		}
	}

}
