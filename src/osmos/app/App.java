package osmos.app;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import osmos.view.GameMenu;

public class App extends Application{

	@Override
	public void start(Stage primaryStage) throws Exception {
		Scene scene = new Scene(new Pane(),1280, 720);
		GameMenu menu = new GameMenu(scene);
		scene.setRoot(menu);
		menu.show();
		primaryStage.setScene(scene);
		primaryStage.show();
	}
	
	public static void main(String[] args){
		Application.launch(args);
	}
}
