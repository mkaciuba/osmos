package osmos.view;


import java.util.Random;

import javafx.scene.Group;
import javafx.scene.effect.BlendMode;
import javafx.scene.effect.BoxBlur;
import javafx.scene.effect.ColorAdjust;
import javafx.scene.effect.DropShadow;
import javafx.scene.effect.Glow;
import javafx.scene.effect.InnerShadow;
import javafx.scene.effect.MotionBlur;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import osmos.model.Cell;
public class CellView extends Group {
	private static final double ROTATE_SPEED_LIMIT = 0.2;
	private static final ImagePattern nebula = new ImagePattern(new Image(CellView.class.getResourceAsStream("nebula.jpg")));
	private static final Random r = new Random();
	private Cell cell;
	private Circle circle0;
	private Circle circle1;
	private Circle circle2;
	private Circle circle3;
	private Rectangle rect;
	private MotionBlur motionBlur = new MotionBlur();
	private BoxBlur blur = new BoxBlur();
	private InnerShadow innerShadow = new InnerShadow();
	private DropShadow dropShadow = new DropShadow();
	private double rSpeed1 = (r.nextDouble()*2 - 1.0) * ROTATE_SPEED_LIMIT;
	private double rSpeed2 = (r.nextDouble()*2 - 1.0) * ROTATE_SPEED_LIMIT;
	public CellView(Cell cell){
		this.cell = cell;
		this.setLayoutX(cell.x());
		this.setLayoutY(cell.y());
		Glow glow = new Glow(0.5);
		glow.setInput(motionBlur);
		ColorAdjust darken = new ColorAdjust();
		darken.setBrightness(-0.3);
		innerShadow.setChoke(0.4);
		dropShadow.setSpread(0.9);

		circle0 = new Circle(0, 0, cell.radius());
		circle0.setFill(Color.GREY);
		circle0.setEffect(blur);
		
		circle1 = new Circle(0, 0, cell.radius());
		circle1.setFill(nebula);
		circle1.setEffect(darken);
		circle1.setBlendMode(BlendMode.SRC_ATOP);
		
		circle2 = new Circle(0, 0, cell.radius());
		circle2.setFill(nebula);
		circle2.setEffect(innerShadow);
		circle2.setBlendMode(BlendMode.SCREEN);
		
		circle3 = new Circle(0,0, cell.radius());
		circle3.setFill(Color.TRANSPARENT);
		circle3.setStroke(Color.LIGHTGRAY);
		circle3.setStrokeWidth(0.2);
		circle3.setEffect(dropShadow);
		
		rect = new Rectangle();
		rect.setFill(Color.TRANSPARENT);
		this.getChildren().addAll(rect,circle0,circle1,circle2,circle3);
		this.setEffect(glow);
		update();
	}
	
	public void update(){
		this.setLayoutX(cell.x());
		this.setLayoutY(cell.y());
		circle0.setRadius(cell.radius()*0.7);
		circle1.setRadius(cell.radius()*0.9);
		circle1.setRotate(circle1.getRotate() + rSpeed1);
		circle2.setRadius(cell.radius());
		circle2.setRotate(circle2.getRotate() + rSpeed2);
		circle3.setRadius(cell.radius());
		rect.setHeight(cell.radius()*2.2);
		rect.setWidth(cell.radius()*2.2);
		rect.setX(-cell.radius()*1.1);
		rect.setY(-cell.radius()*1.1);
		if(Math.abs(cell.vx()) > 0 || Math.abs(cell.vy()) > 0){
			motionBlur.setAngle(Math.toDegrees(Math.atan(cell.vy()/cell.vx())));
		}
		motionBlur.setRadius(Math.sqrt(cell.vx()*cell.vx() + cell.vy()*cell.vy())/60);
		blur.setHeight(cell.radius()*0.3);
		blur.setWidth(cell.radius()*0.3);
		innerShadow.setHeight(cell.radius()*0.7);
		innerShadow.setWidth(cell.radius()*0.7);
		innerShadow.setColor(cell.color().value());
		dropShadow.setHeight(cell.radius()*0.3);
		dropShadow.setWidth(cell.radius()*0.3);
		dropShadow.setColor(cell.color().value());
	}
}
