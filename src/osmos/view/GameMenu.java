package osmos.view;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;

import javafx.scene.Scene;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ScrollPane.ScrollBarPolicy;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import osmos.controller.MainController;
import osmos.controller.SimpleRules;
import osmos.leveleditor.LevelEditor;
import osmos.model.Cell;

public class GameMenu extends Pane{
	private final static String LEVELS_DIR = "./levels";
	private double marginTop = 200.0;
	private double verticalSpace = 80.0;
	private Scene scene;
	public GameMenu(Scene scene){
		this.scene = scene;
		this.setBackground(new Background(new BackgroundFill(Color.BLACK, null, null)));
	}
	
	public void show(){
		GameMenu.this.getChildren().clear();
		addItem(this, new GameMenuItem("START", ()->{
			GameMenu.this.getChildren().clear();
			
			File levelsDir = new File(LEVELS_DIR);
			
			FileFilter filter = new FileFilter() {
				@Override
				public boolean accept(File pathname) {
					return pathname.getName().endsWith(".lvl");
				}
			};
			Pane pane = new Pane();
			ScrollPane scrollPane = new ScrollPane();
			scrollPane.setMaxHeight(GameMenu.this.getHeight());
			scrollPane.setBackground(new Background(new BackgroundFill(Color.BLACK, null, null)));
			scrollPane.layoutXProperty().bind(GameMenu.this.widthProperty().divide(2).subtract(scrollPane.widthProperty().divide(2)));
			scrollPane.setPrefWidth(600);
			scrollPane.setVbarPolicy(ScrollBarPolicy.NEVER);
			scrollPane.setHbarPolicy(ScrollBarPolicy.NEVER);
			pane.prefWidthProperty().bind(scrollPane.widthProperty());
			pane.setBackground(new Background(new BackgroundFill(Color.BLACK, null, null)));
			GameMenu.this.getChildren().add(scrollPane);
			scrollPane.setContent(pane);
			for(File file : levelsDir.listFiles(filter)){
				try(BufferedReader reader = Files.newBufferedReader(file.toPath())) {
					String name = reader.readLine();
					GameMenu.this.addItem(pane, new GameMenuItem(name, () ->{
						try {
							start(file);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}));
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			GameMenu.this.addItem(pane, new GameMenuItem("Back", () ->{
				GameMenu.this.show();
			}));
			
		}));
			addItem(this, new GameMenuItem("LEVEL CREATOR",()->{
			LevelEditor levelEditor = new LevelEditor(this, scene);
			scene.setRoot(levelEditor);
		}));
	}
	
	private void start(File file) throws IOException {
		try(BufferedReader reader = Files.newBufferedReader(file.toPath())) {
			reader.readLine();
			Cell playerCell = getPlayerCell(reader);
			ArrayList<Cell> cells = loadLevel(reader);
			Board board = new Board(1280, 720);
			scene.setRoot(board);
			MainController mc = new MainController(board, this, scene, new SimpleRules(), cells, playerCell);
			mc.start();
		}
	}

	private Cell getPlayerCell(BufferedReader reader) throws IOException {
		return Cell.parse(reader.readLine());
	}

	private ArrayList<Cell> loadLevel(BufferedReader reader) throws IOException {
		ArrayList<Cell> cells = new ArrayList<>();
		String line = null;
		while((line=reader.readLine()) != null){
			cells.add(Cell.parse(line));
		}
		return cells;
	}

	private void addItem(Pane pane, GameMenuItem menuItem){
		double y = 0;
		if(pane.getChildren().isEmpty()){
			y = marginTop;
		}else{
			y = pane.getChildren().get(pane.getChildren().size() - 1).getLayoutY() + verticalSpace;
		}
		pane.getChildren().add(menuItem);
		menuItem.setLayoutY(y);
		menuItem.layoutXProperty().bind(pane.widthProperty().multiply(0.5));
	}
	
}
