package osmos.view;

import javafx.beans.property.DoubleProperty;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.control.Label;
import javafx.scene.effect.Glow;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;

public class GameMenuItem extends Group{
	private final static String FONT_NAME = "Calibri";
	private final static double FONT_SIZE = 30.0;
	private final static Glow glow = new Glow(0.9);
	private Rectangle rect = new Rectangle();
	public GameMenuItem(String text, Runnable action){
		Label label = new Label();
		label.setText(text);
		label.setFont(new Font(FONT_NAME,FONT_SIZE));
		label.layoutXProperty().bind(label.widthProperty().multiply(-0.5));
		label.layoutYProperty().bind(label.heightProperty().multiply(-0.5));
		label.setTextFill(Color.GREY);
		rect.widthProperty().bind(label.widthProperty().add(20));
		rect.layoutXProperty().bind(rect.widthProperty().multiply(-0.5));
		rect.layoutYProperty().bind(rect.heightProperty().multiply(-0.5));
		rect.setHeight(FONT_SIZE + 20);
		rect.setFill(Color.TRANSPARENT);
		layoutXProperty().bind(rect.widthProperty().multiply(-0.5));
		
		this.getChildren().addAll(rect,label);
		this.addEventFilter(MouseEvent.ANY, new EventHandler<MouseEvent>(){
			@Override
			public void handle(MouseEvent event) {
				if(event.getEventType() == MouseEvent.MOUSE_ENTERED){
					GameMenuItem.this.setEffect(glow);
					label.setTextFill(Color.WHITE);
				}
				else if(event.getEventType() == MouseEvent.MOUSE_EXITED){
					GameMenuItem.this.setEffect(null);
					label.setTextFill(Color.GREY);
				}else if(event.getEventType() == MouseEvent.MOUSE_CLICKED){
					action.run();
				}
			}
		});
	}

	public DoubleProperty widthProperty(){
		return rect.widthProperty();
	}
	
	public DoubleProperty heightProperty(){
		return rect.heightProperty();
	}
}
