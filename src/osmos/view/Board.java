package osmos.view;

import javafx.scene.control.Label;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import osmos.controller.GameStatus;
import osmos.model.Cell;

public class Board extends Pane{
	private final static double END_GAME_FONT_SIZE = 40.0;
	private int width;
	private int height;
	private Label label = new Label();
	private final static String LABEL_TEXT = "EFFECT: %-20sTIME: %.2f";
	public Board(int width, int height){
		this.width = width;
		this.height = height;
		this.setBackground(new Background(new BackgroundFill(Color.BLACK, null, null)));
		this.getChildren().add(label);
		label.setLayoutX(50);
		label.setLayoutY(50);
		label.setFont(new Font(20));
		label.setTextFill(Color.WHITE);
	}
	public double width() {
		return width;
	}
	public double height() {
		return height;
	}
	
	public void updateStatus(Cell playerCell){
		this.getChildren().remove(label);
		this.getChildren().add(label);
		label.setText(String.format(LABEL_TEXT, playerCell.effect().toString(),playerCell.effectTime()));
	}
	
	public void endGame(GameStatus gameStatus){
		Rectangle rect = new Rectangle();
		rect.setWidth(width);
		rect.setHeight(height);
		rect.setFill(new Color(0,0,0,0.5));
		Label label = new Label();
		label.setFont(new Font(END_GAME_FONT_SIZE));
		label.setTextFill(Color.WHITE);
		if(gameStatus == GameStatus.WIN)
			label.setText("YOU WIN!");
		else
			label.setText("YOU LOOSE!");
		label.layoutXProperty().bind(this.widthProperty().divide(2).subtract(label.widthProperty().divide(2)));
		label.layoutYProperty().bind(this.heightProperty().divide(2).subtract(label.heightProperty().divide(2)));
		this.getChildren().addAll(rect,label);
		
	}
}
