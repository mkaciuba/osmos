
public class t {

	public static void main(String[] args) {
		double r = 100;
		double x,y;
		for(int i=0;i<360;i++){
			x = r * Math.cos(Math.toRadians(i));
			y = r * Math.sin(Math.toRadians(i));
			System.out.println(x + " " + y + " " + Math.toDegrees(Math.atan(x/y)));
		}
	}

}
